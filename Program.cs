﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareWithStorage
{
    class Program
    {
        const string destPath = "D:\\storage\\apatynowski";
        static void Main(string[] args)
        {
            // args[0] - ścieżka skąd skopiować plik4
            // args[1] - nazwa folderu do jakiego plik ma być skopiowany
            if(args != null)
            {
                if(args.Length == 1)
                {
                    Console.WriteLine(Copy(args[0]));
                }
                else
                {                    
                    Console.WriteLine(Copy(args[0], args[1]));
                }
            }
            else
            {
                throw new Exception("Nie prawidłowa ilość parametrów");
            }
        }

        static string Copy(string path, string destFolder = "")
        {
            // Tworzę niezbędne zmienne
            string destination = !String.IsNullOrEmpty(destFolder) ? destPath + destFolder : destPath;

            if (path == null)
            {
                throw new Exception("Nie podano ścieżki");
            }
            else
            {
                // Ustalam, czy kopiuje plik, czy folder
                bool isFile = File.Exists(path);
                bool isDirectory = Directory.Exists(path);

                if (!(isFile || isDirectory))
                {
                    throw new Exception("Podana ścieżka nie wskazuje na plik lub folder");
                }

                //Sprawdzam czy ścieżka do ktróej kopiuje istnieje jeżeli nie to znaczy, że chcemy skopiowac do określonego folderu i muszę go stworzyć
                if (!Directory.Exists(destination))
                {
                    try
                    {
                        Directory.CreateDirectory(destination);
                    }
                    catch (Exception e)
                    {
                        return "Nie udało się utworzyć folderu docelowego. Udostępnianie zakończyło się niepowodzeniem.";
                    }
                }

                if (isFile)
                {
                    try
                    {
                        File.Copy(path, destination, true);
                        return "Kopiowanie zakończone oto ścieżka do pliku: " + destination;
                    }
                    catch (Exception e)
                    {
                        return "Nie udało się skopiować pliku. Udostępnianie zakończyło się niepowodzeniem.";
                    }
                }

                if (isDirectory)
                {
                    try
                    {
                        CopyFolder(path, destination);
                    }
                    catch (Exception e)
                    {
                        return "Nie udało się skopiować folderu. Udostępnianie zakończyło się niepowodzeniem.";
                    }

                }
            }

            return "Skopiowano do " + destination;
        }

        static void CopyFolder(string sourceFolder, string destFolder)
        {
            if(!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }

            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }

            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }
    }
}
